#!/usr/bin/perl
use v5.30;
use warnings;
use strict;

# Very hacky check for parens without spaces

my $src_file = $ARGV[0];
my $src_file_name =  $src_file;
if ( $src_file !~/\.tal$/) {
    $src_file.='.tal';
} else {
$src_file_name=~s/\.tal//;
}
my @src_files = ( $src_file ) ;

while ( my $src_file = pop @src_files) { 
    # say STDERR "*** Checking $src_file";
open my $TAL, '<', $src_file or die "$src_file: $!";
while (my $line=<$TAL>) {
    chomp $line;
    $line=~/~([\w\.\-]+)/ && do {
        push @src_files, $1;
    };
    if ($line=~/\(\S/ or $line =~/\S\)/ ) {
        my $line_cleaned=$line;
        if ( $line_cleaned=~/\(.+\(.+\).+\)/) {
        for (1..10) { # ad hoc, just assuming there will be no more than 5 paren pairs inside a comment
        $line_cleaned=~s/\(\S.+?\)//;
        $line_cleaned=~s/\(.+?\S\)//;
        }
        }
        if ($line_cleaned=~/\(\S/ or $line_cleaned =~/\S\)/ ) {
            say STDERR "*** In $src_file: ";
            say STDERR "\tParen without space on line: $line";
        }
    }
}

close $TAL;

}

print $src_file_name;
