# hyakuwa -- 兎百羽

Repo for [Uxntal](https://wiki.xxiivv.com/site/uxntal.html) and [Uxn](https://wiki.xxiivv.com/site/uxn.html) code. 

## `common-macros`

These are macros that I use in most of my code. 

- `keywords` has macros `call`, `return` etc.
- `nicer` has macros for all hex digits and for the operators corresponding to the opcodes, e.g.  

    #0006 #0007 MUL2 => 6 7 * 

- `vars` defines access to `x`, `y` and `z` global variables, e.g. 

    .x STZ2 .x LDZ2 => ->x x

- `print` defines various macros for printing, it uses the `print.tal` library in `common-libs`

## `utils`

Contains `asm-emu.sh`, a script to assemble and run, with a naive comments checker.

## `quoting-lamdas` 

This subdirectory contains the macros and libraries for my implementation of quoting and lambda functions.

## `sketches`

Just random bits of code for trying things out and testing.
