# Quoting and lambda functions

This subdirectory contains the macros and libraries for my implementation of quoting and lambda functions (anonymous functions). There are two implementations, a 16-bit one and an 8-bit one. The 8-bit version is only 8-bits in terms of variables, constants and numeric operations, internally all values are stored as 16-bit words. 

Essentially, we build anonymous functions from "quoted" symbols. A quoted symbol is not evaluated. A special function `apply` performs the actual evaluation of the anonymous function. 

For more details, see [my blog post "Functional programming in stack-based assembly"](https://wimvanderbauwhede.github.io/articles/uxntal-quoting/).

The best way to see how this works is to look at the examples `test_quote-lambda.tal` and `test_quote-lambda_8bit.tal`. For example:

    5 \[ \x. x' x' *' \[ \y. y' 1' +' \] apply' \] apply ( returns #001a )

Currently, I have implemented macros for x, y and z and for digits 0 .. A but it is quite straightforward to extend or customise. 

Lambdas can be nested, and used as values: every non-nested lambda returns its own address.

Quoting essentially allows to defer evaluation of any operation until later, so it can be used for example to build conditional operations etc. 

As examples, the `test_tuple.tal` and `test_cons-list.tal` show the use of tuples and cons lists implemented with this machinery (in `tuple.tal` and `cons-list.tal`).

## Macros and libraries

The code relies heavily on macros for abstraction. I use the convention that a library file has a corresponding macros file with the `_macros` suffix, and a further `_8bit` suffix for the 8-bit implementation, for example:

    quote-lambda_macros_8bit.tal
    quote-lambda_macros.tal
    quote-lambda.tal

The macro files are to be included before the main program, their corresponding library files after the main program. For example, `test_quote-lambda_minimal.tal`:


    ~../common-macros/print_macros.tal
    ~../common-macros/keywords_macros.tal ( for call, return etc )

    ( the actual lambda/quoting functionality )
    ~lambda-stack_macros.tal
    ~quote-lambda_macros.tal
    ~unquote-lambda_macros.tal    

    ~lambda_decls.tal

    |0100
    init-lambda-stack
    [' #002a \' ]' apply putc-nl ( prints * )

    BRK

    ~../common-libs/print.tal

    ~lambda-stack.tal
    ~quote-lambda.tal
    ~unquote-lambda.tal
    ~lambda-storage.tal

## Memory allocation    

The files `lambda_decls.tal` and `lambda-storage.tal` must always be included in their respective locations:  

* `lambda_decls.tal` contains the start of the zero page (`|0000`), so any remaining zero page allocation should go after it. 
*  `lambda-storage.tal` is the location where the quoted expressions are stored, this should always the the final label in the program.